<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $customer_id
 * @property string $amount
 * @property string $type
 *
 * @package App
 */
class Agreement extends Model
{
    const TYPE_WEEKLY = 'weekly';
    const TYPE_MONTHLY = 'monthly';

    protected $fillable = [
        'unit_price', 'type'
    ];


    public function invoices()
    {
      return $this->hasMany(Invoice::class);
    }

    public function customer()
    {
      return $this->hasOne(Customer::class);
    }

    public function getTypeDaysAttribute()
    {
      if($this->type == self::TYPE_WEEKLY) return 7;
      if($this->type == self::TYPE_MONTHLY) return 30;
      return 0;
    }

}
