<?php

namespace App\Http\Controllers;

use App\Invoice;
use Illuminate\Support\Facades\View;

class InvoiceController
{
    public function index()
    {
        $invoices = Invoice::with('agreement', 'agreement.customer')->get();
        return view('invoices', compact('invoices'));
    }

}
