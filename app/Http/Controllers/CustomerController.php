<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Invoice;
use App\Delivery;
use Carbon\Carbon;


/**
* I choose to respect the given architecture, or the lack here of, as stated in the Assignment notes.
* Best practice to only let classes in the Http folder handle responsibility concerning requests(routing, validation, request in out)
* Have dedicated classes for all business logic (Services) and classes dedicated to db and eloquent (Repositories)
* Dependency inject these into controller classes where needed, and let them handle all the logic not concerning the request.
*
* I also think the model classes should be organized in folder instead off app root
*/

class CustomerController extends Controller
{

    public function index()
    {
        $customers = Customer::all();

        //use helpers instead of importing facade - cleaner
        return view('customers', compact('customers'));
    }

    public function show($id)
    {
        $customer = Customer::findOrFail($id);
        $invoices = $customer->agreement->invoices;

        return view('customer', compact('customer', 'invoices'));
    }

    public function invoice($id)
    {


        //eager load agreement relation
        $customer = Customer::with(['agreement'])->findOrFail($id);

        //normally i would'nt have this kind of logic in a controller method as stated in the comments for the class
        $invoice = $this->createInvoiceFromCustomer($customer);

        //basic error handling
        if(!$invoice){
          return back()->withErrors(['An error has occurred trying to invoice customer']);
        }

        //no need to redirect -> re-route and bootstrap everything all over again. Just call the method directly.
        return $this->show($id);
    }

    public static function createInvoiceFromCustomer(Customer $customer)
    {
      $now = Carbon::now();

      //get delivery count sum based on users agreement period
      $deliveriesCountSum = Delivery::where('customer_id', $customer->id)
                                    ->whereDate('delivered_at', '>=', $now->subDay($customer->agreement->type_days)->format('Y-m-d'))
                                    ->sum('count');

      $invoiceAmount = $deliveriesCountSum * $customer->agreement->unit_price;

      //create invoice
      $invoice = Invoice::create([
        'agreement_id' => $customer->agreement->id,
        'invoice_no' => $now->timestamp, // could also just be the unique id
        'amount' => $invoiceAmount,
        'invoice_due_at' => $now->addDay(14)
      ]);

      return $invoice;

    }
}
