<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $invoice_no
 * @property string $invoice_due_at
 * @property string $amount
 *
 * @package App
 */
class Invoice extends Model
{
    protected $fillable = [
        'agreement_id',
        'invoice_no',
        'invoice_due_at',
        'amount'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'invoice_due_at'
    ];


    public function agreement()
    {
      return $this->belongsTo(Agreement::class);
    }

    public function getCustomerAttribute()
    {
      return isset($this->agreement->customer) ? $this->agreement->customer : NULL;
    }
}
