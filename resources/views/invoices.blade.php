@extends('layout')
<?php /** @var $invoices \App\Invoice[]  */ ?>
@section('content')
    <h2>Invoices</h2>
    <div class="table-responsive">
      @include('invoices_table')
    </div>
@endsection
