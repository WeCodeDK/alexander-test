<table class="table table-striped">
    <thead>
    <tr>
        <th>#</th>
        <th>Due</th>
        <th>Amount</th>
        @if(!isset($customer))
          <th>Customer</th>
        @endif

    </tr>
    </thead>
    <tbody>
    @foreach($invoices as $invoice)
    <tr>
        <td>{{ $invoice->invoice_no}}</td>
        <td>{{ $invoice->invoice_due_at }}</td>
        <td>{{ $invoice->amount}}</td>
        @if(!isset($customer))
        <td>
          <a href="/customer/{{$invoice->customer->id}}">{{$invoice->customer->name}}</a>
        </td>
        @endif

    </tr>
    @endforeach
    </tbody>
</table>
