<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Laravel</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>

<body>


  <div id="app">

    <nav class="navbar fixed-top navbar-expand-md navbar-dark bg-primary mb-3">
      <div class="flex-row d-flex">
        <button type="button" class="navbar-toggler mr-2 " data-toggle="offcanvas" title="Toggle responsive left sidebar">
          <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#" title="Free Bootstrap 4 Admin Template">Obviux Assignment</a>
      </div>
    </nav>

    <div class="container-fluid" id="main">
      <div class="row row-offcanvas row-offcanvas-left">
        <div class="col-md-3 col-lg-2 sidebar-offcanvas bg-light pl-0 h-100" id="sidebar" role="navigation">
          <ul class="nav flex-column sticky-top pl-0 pt-5 mt-4">
            <li class="nav-item">
              <a class="nav-link" {{ ( (Route::currentRouteName() == 'customers') ? 'id=active_menu' : '') }}
              href="/">Customers </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" {{ ( (Route::currentRouteName() == 'invoices') ? 'id=active_menu' : '') }}
              href="/invoices">Invoices</a>
            </li>
          </ul>
        </div>
        <!--/col-->

        <div class="col main pt-5  h-100">
          <div class="container mt-4">
            @yield('content')
          </div>
        </div>

      </div>

    </div>

  </div>

  <script src="{{ mix('/js/app.js') }}"></script>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <!-- These are already loaded and compiled in app.js -->
  <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script> -->


</body>

</html>
