@extends('layout')
<?php /** @var $customer \App\Customer  */ ?>
@section('content')
    <h2>{{$customer->name}}</h2>

    <div class="row">
      <div class="col-md-6">
        <div class="mb-2">DKK{{$customer->agreement->amount}} {{$customer->agreement->type}}</div>
      </div>
      <div class="col-md-6">
        <form method="post" action="/customer/invoice/{{$customer->id}}">
            {{ csrf_field() }}
            <input type="submit" class="btn btn-success float-right" value="Invoice customer" />
        </form>
      </div>
    </div>


    @if($errors->any())
    <h4 class="pt-4 text-danger">{{$errors->first()}}</h4>
    @endif

    <div class="pt-4">
      @include('invoices_table')
    </div>


@endsection
