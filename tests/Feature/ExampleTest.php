<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Agreement;
use App\Customer;
use App\Delivery;
use App\Invoice;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{

  /**
   * @var Customer
   */
  private $customer;

    public function setUp()
    {
        parent::setUp();

        $this->customer = factory(Customer::class)->create([
            'name' => 'Søren Petersen',
            'agreement_id' => factory(\App\Agreement::class)->create([
                'unit_price' => 12.00,
                'type' => Agreement::TYPE_WEEKLY,
            ])->id,
        ]);

        factory(Delivery::class)->create([
            'delivered_at' => Carbon::now()->subDays(3),
            'count' => 5,
            'customer_id' => $this->customer->id,
        ]);
        factory(Delivery::class)->create([
            'delivered_at' => Carbon::now()->subDays(8),
            'count' => 2,
            'customer_id' => $this->customer->id,
        ]);

        factory(Invoice::class)->create([
            'agreement_id' => 1 ,
            'invoice_no' => 21,
            'amount' => 50,
            'invoice_due_at' => Carbon::now()->addDays(14),
        ]);
    }


    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertSeeText('Jørgen');
        $response->assertSeeText('Peter');

    }

    public function testInvoiceCreationAndFeedbackTest()
    {
      //check new customer doesn't have any invoices
      $response = $this->get('/customer/'.$this->customer->id);
      $response->assertDontSee('60.00');

      //call create invoice and see check that response now contains invoice
      $response = $this->get('/customer/invoice/'.$this->customer->id);
      $response->assertSeeText('60.00');
    }

    public function testInvoiceIndexTest()
    {
      $response = $this->get('/invoices');
      $response->assertSeeText('Jørgen Hansen');
    }


}
